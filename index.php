<?php

$types = ["Normal", "Fire", "Water", "Grass", "Electric", "Ice", "Fighting", "Poison", "Ground", "Flying", "Psychic", "Bug", "Rock", "Ghost", "Dragon", "Dark", "Steel", "Fairy"];

switch($_GET["page"])
{
    case "types":
        echo "This is a page with the types<br>";

        for($i = 0; $i < count($types); $i++)
        {
            echo '<a href="types/' . strtolower($types[$i]) . '">' . $types[$i] . ' Moves</a><br>';
        }
        break;
    case "typemoves":
        $usertype = $_GET["type"];
        $files = scandir("moves");

        for($i = 0; $i < count($files); $i++)
        {
            if($files[$i] == "." || $files[$i] == "..")
                continue;

            $contents = file_get_contents("moves/" . $files[$i]);

            $move = json_decode($contents);

            if(strtolower($move->Type) == $usertype)
                echo '<a href="' . $usertype . '/' . str_replace(".json", "", $files[$i]) . '">' . $move->Name . '</a> (' . $move->Type . ", " . $move->Category . ')<br>';
        }
        break;
    case "movedetails":
        $movename = $_GET["movename"];
        $contents = file_get_contents("moves/" . $movename . ".json");

        $move = json_decode($contents);

        echo $move->Name . " <i>(";
        echo $move->Type . ", ";
        echo $move->Category . ")</i><br>PP: ";
        echo $move->PPStart . "/";
        echo $move->PPMax . "<br>";
        echo "Accuracy: " . $move->Accuracy . (is_string($move->Accuracy)? "" : "%") . "<br>";
        echo "Power: " . $move->Power . (is_string($move->Power)? "" : "d10") . "<br>Point Cost: ";
        echo $move->PointCost . "<br>";

        if(isset($move->Priority))
        {
            echo "Priority: " . $move->Priority . "<br>";
        }

        echo "<ul>";
        for($i = 0; $i < count($move->Requirements); $i++)
        {
            echo "<li>" . $move->Requirements[$i] . "</li>";
        }
        echo "</ul><i>";
        echo $move->Description . "</i><br>";
        break;
}